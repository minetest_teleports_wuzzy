teleports
=========

Mod "teleports" for Minetest 0.4.17 or later. Updated version of lag01's Teleports mod!

Allows players to create teleports with crafting (8 diamonds + 1 obsidian).

A teleport is a block that teleports players to another nearby teleport (less than 260 blocks distance)
by standing on it.
The teleport will target the teleport (in range) that best matches your viewing direction.

If you 'use' a teleport (rightclick or double-tap),  you can spend mossy cobblestone to
instantly teleport to one of the 6 nearest teleports.

Teleports can be named. If you place a teleport next to a Wooden Sign or Steel Sign,
the teleport will take the name of the sign (can only be done once, max. length 16
characters).

Teleports are sometimes erratic. There's a small chance that a teleport will not put you on
the destination exactly, but one block 'besides' it.

Beware: If you dig a teleport, you only get 1 diamond back, so think before placing!

Version
#######
1.0.0

Credits:
========
This mod code is licensed under the GNU LGPLv2
<https://www.gnu.org/licenses/old-licenses/lgpl-2.0>.

lag01 and rnd get credit for the original code.
Wuzzy gets credit for bugfixes and some gameplay tweaks.

Texture license:

teleports_teleport_top.png
    * License: CC BY-SA 3.0 <http://creativecommons.org/licenses/by-sa/3.0/>
    * Authors: Zeg9, lag01

Sound licenses:

teleports_charge.ogg
    * License: CC0 <http://creativecommons.org/publicdomain/zero/1.0/>
    * Author: squidge316
    * Changes were made (slower speed)
    * Source: <https://freesound.org/people/squidge316/sounds/161336/>

teleports_teleport.ogg
    * License: CC BY 3.0 <http://creativecommons.org/licenses/by/3.0/>
    * Author: tim.kahn
    * Changes were made (higher pitch)
    * Source: <https://freesound.org/people/tim.kahn/sounds/128590/>
